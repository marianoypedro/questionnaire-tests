import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SeleniumTest {

	WebDriver driver;

	@BeforeAll
	static void setupClass() {
		WebDriverManager.chromedriver().setup();
	}

	@BeforeEach
	void setupTest() {
		driver = new ChromeDriver();
	}

	@AfterEach
	void teardown() {
//        driver.quit();
	}

	@Test
	void test() {
			String emailTest = "asd@asd.com";
			driver.get("http://localhost:3000/");	
			driver.findElement(By.id("email-field")).sendKeys(emailTest + Keys.ENTER);
			driver.get("http://localhost:8080/api/User/getUser");
			String emailsAPI = driver.findElements(By.tagName("/html/body/pre")).toString();
			boolean result = emailsAPI.contains(emailTest);
			assertEquals(true, result);
	}

}